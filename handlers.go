package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"math/rand"
	"net/http"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"github.com/satori/go.uuid"
)

// PhoneNumber type representing table fields in database
type PhoneNumber struct {
	PhoneCountryCall string
	PhonePrefix      string
	PhoneNumber      string
}

// User type representing table fields in database
type User struct {
	Phone     PhoneNumber
	Allowable bool
	Found     bool
	HasData   bool
	FirstName string
	LastName  string
	Email     string
	UserID    int
}

var users map[string]*User
var sessions map[string]string

var tmpl *template.Template
var db *sql.DB

var source = rand.NewSource(time.Now().UnixNano())
var r = rand.New(source)

func init() {
	var err error

	db, err = sql.Open("postgres", "postgres://uxshkneo:DC2MS-tEORkDMlsSDHIYHbp-Qf30WidJ@horton.elephantsql.com:5432/uxshkneo")
	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	fmt.Println("init(): Connected to postgres database!")

	users = map[string]*User{}
	sessions = make(map[string]string)

	tmpl = template.Must(template.ParseGlob("templates/*.html"))
}

// Index representing home page
func Index(w http.ResponseWriter, r *http.Request) {
	// Check does session as cookie exist
	// if does not create new one
	cookie, err := r.Cookie("session")
	if err != nil {
		id, _ := uuid.NewV4()

		cookie = &http.Cookie{
			Name:     "session",
			Value:    id.String(),
			HttpOnly: true,
			Path:     "/",
		}

		http.SetCookie(w, cookie)
	}

	tmpl.ExecuteTemplate(w, "index.html", nil)
}

// InsertPhoneNumber into database
func InsertPhoneNumber(w *http.ResponseWriter, number *PhoneNumber) {
	_, err := db.Exec("INSERT INTO phone_numbers (phone_country_call, phone_prefix, phone_number) VALUES ($1, $2, $3)", number.PhoneCountryCall, number.PhonePrefix, number.PhoneNumber)
	if err != nil {
		http.Error(*w, http.StatusText(500), 500)
		return
	}

	fmt.Println("InsertPhoneNumber(): Inserted new phone number!")
}

// UserHasData checks does user has dat
func UserHasData(w *http.ResponseWriter, user *User, userID int) bool {
	row := db.QueryRow("SELECT first_name, last_name, email FROM users WHERE phone_id = $1", userID)

	err := row.Scan(&user.FirstName, &user.LastName, &user.Email)
	switch {
	case err == sql.ErrNoRows:
		return false
	case err != nil:
		panic(err)
	}

	return true
}

// CheckNumber either inserts a phone number into database
// or if phone number exists then takes user to enter code
// he/she got on mobile
func CheckNumber(w http.ResponseWriter, r *http.Request) {
	number := PhoneNumber{}

	number.PhoneCountryCall = r.FormValue("phone_country_call")
	number.PhonePrefix = r.FormValue("phone_prefix")
	number.PhoneNumber = r.FormValue("phone_number")

	// Check is number in table
	row := db.QueryRow("SELECT * FROM phone_numbers WHERE phone_number = $1", number.PhoneNumber)

	data := User{}
	err := row.Scan(&data.UserID, &data.Phone.PhoneCountryCall, &data.Phone.PhonePrefix, &data.Phone.PhoneNumber, &data.Allowable)

	switch {
	case err == sql.ErrNoRows:
		fmt.Println("CheckNumber(): Phone number does not exist!")
		InsertPhoneNumber(&w, &number)
		http.Redirect(w, r, "/confirmation", 303)
		return
	case err != nil:
		fmt.Println("CheckNumber(): Error occured during scaning data from database!")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Println("CheckNumber(): Data is found!")
	data.Found = true

	cookie, err := r.Cookie("session")
	if err != nil {
		http.Redirect(w, r, "/", 303)
		return
	}

	data.HasData = UserHasData(&w, &data, data.UserID)

	sessions[cookie.Value] = data.Phone.PhoneNumber
	users[data.Phone.PhoneNumber] = &data

	http.Redirect(w, r, "/confirmation", 303)
}

// CheckUserUpdates user details based on session if something has changed in database
func CheckUserUpdates(user *User) {
	var err error
	rowPhoneNumber := db.QueryRow("SELECT allowable FROM phone_numbers WHERE phone_number = $1", user.Phone.PhoneNumber)

	data := User{}
	err = rowPhoneNumber.Scan(&data.Allowable)
	if err != nil {
		return
	}

	if user.Allowable != data.Allowable {
		user.Allowable = data.Allowable
		fmt.Println("CheckUserUpdates(): Allowable is updated!")
	}

	rowUser := db.QueryRow("SELECT first_name, last_name, email FROM users WHERE phone_id = $1", user.UserID)
	err = rowUser.Scan(&data.FirstName, &data.LastName, &data.Email)
	if err != nil {
		return
	}

	if data.FirstName != user.FirstName {
		user.FirstName = data.FirstName
		fmt.Println("CheckUserUpdates(): First name is updated!")
	}

	if data.LastName != user.LastName {
		user.LastName = data.LastName
		fmt.Println("CheckUserUpdates(): First name is updated!")
	}

	if data.Email != user.Email {
		user.Email = data.Email
		fmt.Println("CheckUserUpdates(): Email is updated!")
	}
}

// Confirmation prompts user to enter code he/she got on mobile
// or outputs information message
func Confirmation(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("session")

	if err != nil {
		http.Redirect(w, r, "/", 303)
	} else {
		user := users[sessions[cookie.Value]]

		CheckUserUpdates(user)
		SendConfirmationCode(users[sessions[cookie.Value]].UserID)

		tmpl.ExecuteTemplate(w, "confirmation.html", user)
	}
}

// UpdateUserDetails updates user information in database
func UpdateUserDetails(w http.ResponseWriter, r *http.Request) {
	FirstName := r.FormValue("first_name")
	LastName := r.FormValue("last_name")
	Email := r.FormValue("email")

	cookie, _ := r.Cookie("session")
	users[sessions[cookie.Value]].FirstName = FirstName
	users[sessions[cookie.Value]].LastName = LastName
	users[sessions[cookie.Value]].Email = Email

	// Even though function is named update, what it does it inserts into users table
	_, err := db.Exec("INSERT INTO users (phone_id, first_name, last_name, email) VALUES ($1, $2, $3, $4)", users[sessions[cookie.Value]].UserID, FirstName, LastName, Email)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
}

// GenerateCode generates code consisting of n digits
func GenerateCode(n int) string {
	const chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	var code strings.Builder

	for i := 0; i < n; i++ {
		code.WriteString(string(chars[r.Intn(len(chars))]))
	}

	return code.String()
}

// SendConfirmationCode to phone (this is under development, store only in database)
func SendConfirmationCode(UserID int) {
	code := GenerateCode(4)

	_, err := db.Exec("INSERT INTO codes (phone_id, code) VALUES ($1, $2)", UserID, code)
	if err != nil {
		panic(err)
	}
}
