CREATE TABLE phone_numbers
(
    id SERIAL PRIMARY KEY,
    phone_country_call CHAR(3) NOT NULL,
    phone_prefix CHAR(3) NOT NULL,
    phone_number CHAR(7) NOT NULL,
    allowable BOOLEAN DEFAULT(false),
    UNIQUE(phone_number)
);

CREATE TABLE users
(
    id SERIAL PRIMARY KEY,
    phone_id INTEGER REFERENCES phone_numbers(id),
    first_name CHAR(30),
    last_name CHAR(30),
    email TEXT,
    UNIQUE(email)
);

CREATE TABLE codes
(
    id SERIAL PRIMARY KEY,
    phone_id INTEGER REFERENCES phone_numbers(id),
    code CHAR(4)
);