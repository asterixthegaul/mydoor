package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(http.Dir("templates/css/"))))

	r.HandleFunc("/", Index).Methods("GET")
	r.HandleFunc("/check/number", CheckNumber).Methods("POST")
	r.HandleFunc("/confirmation", Confirmation).Methods("GET")
	r.HandleFunc("/user/update", UpdateUserDetails).Methods("POST")

	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":8080", r))
}
